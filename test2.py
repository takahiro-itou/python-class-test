#!/usr/bin/python3

import mymodules.piyo

class HogeA:
    def __init__(self, v):
        print('Init HogeA')
        self.value = v

    def print_value(self):
        print("HogeA : value = {0}".format(self.value,))

class FugaB:
    def __init__(self, v):
        print('Init FugaB')
        self.value = v

    def print_value(self):
        print("FugaB : value = {0}".format(self.value,))

def instantiate(cls, val):
    return  cls(val)

x1 = instantiate(HogeA, 1234)
x1.print_value()

x2 = instantiate(FugaB, 555)
x2.print_value()


print(globals())
y1 = eval('HogeA')
print(y1)

y2 = eval('HogeA(512)')
print(y2)
y2.print_value()

y3 = eval('mymodules.piyo.PiyoPiyoC')
# y4 = eval('PiyoPiyoC')
print(y3)
x3 = instantiate(y3, 100)
x3.print_value()
